;; Needed to load themes
(package-initialize)
;; --------------------------------------------------------------------------
;; Package Archives
;; --------------------------------------------------------------------------
(require 'package)
(add-to-list 'package-archives
             ;;  '("melpa" . "http://melpa.milkbox.net/packages/") t)
               '("melpa" . "http://melpa.org/packages/") t)

;; --------------------------------------------------------------------------
;; GENERAL APPEARANCE
;; --------------------------------------------------------------------------

;; Turn off mouse interface early in startup to avoid momentary display
;; From http://whattheemacsd.com//init.el-01.html
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; No welcome message; you'll get the scratch buffer instead
(setq inhibit-startup-message t)

;; Set the frame height and width at startup
;; check OS type
(cond
 (
  (string-equal system-type "darwin")               ;; Mac OS X
   (add-to-list 'default-frame-alist '(height . 45))
   (add-to-list 'default-frame-alist '(width . 100))
   (set-face-font 'default "Monaco-18")
   (load-theme 'solarized-light t)
   ;; Automatically enable flyspell-mode
   ;; First, set location of Aspell
   (add-to-list 'exec-path "/usr/local/bin")
   (add-hook 'text-mode-hook 'flyspell-mode)
   ;; Who am I?
   ;; Customize this depending on work or home
   (setq user-full-name "Glenn Street"
      user-mail-address "glenn.r.street@gmail.com")
   )
 (
  (string-equal system-type "windows-nt")           ;; Microsoft Windows at work
   (add-to-list 'default-frame-alist '(height . 45))
   (add-to-list 'default-frame-alist '(width . 100))
   (set-face-font 'default "Consolas-12")
   (load-theme 'solarized-light t)
   ;; Automatically enable flyspell-mode
   ;; First, set location of Aspell
   (add-to-list 'exec-path "C:/Program Files (x86)/Aspell/bin/")
   (add-hook 'text-mode-hook 'flyspell-mode)
   ;; Who am I?
   ;; Customize this depending on work or home
   (setq user-full-name "Glenn Street"
      user-mail-address "gstreet@copyright.com")
   )
   )

;; Hightlight the current line
(global-hl-line-mode 1)
;; To customize the highlight line color
(set-face-background hl-line-face "#E0E0E0")

;; Display the full path of a file in the frame title
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

;; --------------------------------------------------------------------------
;; MODE LINE
;; --------------------------------------------------------------------------

;; Display the date and time in the mode line
(setq display-time-day-and-date t)
(display-time)

;; Display the current column number in the mode line
(setq column-number-mode t)

;; ---------------------------------------------------------------------------
;; COLORS
;; --------------------------------------------------------------------------

;; Give me colors in major editing modes!!!!!
(require 'font-lock)

;; ---------------------------------------------------------------------------
;; CUSTOM KEYBINDINGS
;; ---------------------------------------------------------------------------
;; Simulate Eclipse behavior for toggling comments in a region
(global-set-key (kbd "C-<f7>") 'comment-or-uncomment-region)
;; Increase and decrease text scale in all buffers
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; ---------------------------------------------------------------------------
;; PROGRAMMING NICETIES
;; ---------------------------------------------------------------------------

;; Display commented lines in italics

(setq w32-enable-italics t)
(make-face-italic 'font-lock-comment-face)

;; Automatically insert matching "parenthesis"
(electric-pair-mode 1)

;; For Java, include the { and } characters
;; See http://www.emacswiki.org/emacs/ElectricPair
(defun electric-pair ()
      "If at end of line, insert character pair without surrounding spaces.
    Otherwise, just insert the typed character."
      (interactive)
      (if (eolp) (let (parens-require-spaces) (insert-pair)) (self-insert-command 1)))

(add-hook 'java-mode-hook
          (lambda()
	    (define-key java-mode-map "{" 'electric-pair)
	    (define-key java-mode-map "<" 'electric-pair)))

;; Highlight matching parentheses
(show-paren-mode 1)

;; Show line numbers when typing the F6 key
(require 'linum)
(global-set-key (kbd "<f6>") 'linum-mode)

;; Automatically add line numbers to all files when opened
;; This can be toggeled off in a buffer with F6
(global-linum-mode 0)

;; Set tabs at every 4 spaces to follow our standard
(setq tab-stop-list (number-sequence 4 120 4))

;; --------------------------------------------------------------------------
;; TEXT MODE
;; --------------------------------------------------------------------------

;; Assume every file is text, unless it's not (!)
(setq-default major-mode 'text-mode)

;; Automatically enable visual-line-mode (new in Emacs 23+, was longlines-mode)
(add-hook 'text-mode-hook 'visual-line-mode) 

;; --------------------------------------------------------------------------
;; MISCELLANEOUS BEHAVIOR
;; --------------------------------------------------------------------------

;; Use the system "Recycle Bin" when deleting files
(setq delete-by-moving-to-trash t)

;; Use desktop mode
(desktop-save-mode 1)

;; Automatically apply visual-line-mode to text files
(setq auto-mode-alist (cons '("\\.txt$" . visual-line-mode) auto-mode-alist))

;; Automatically apply groovy-mode to .gradle files
;; Requires installation of groovy-mode
(setq auto-mode-alist (cons '("\\.gradle$" . groovy-mode) auto-mode-alist))

;; Assume that we'll be using PostgreSQL when evaluating SQL files
(eval-after-load "sql"
'(progn
(sql-set-product 'postgres)
;; any other config specific to sql
))

;; Keep track of recently visited files automatically
(require 'recentf)
(setq recentf-max-saved-items 200
      recentf-max-menu-items 15)
(recentf-mode +1)

;; --------------------------------------------------------------------------
;; Org-Mode Setup
;; --------------------------------------------------------------------------
;; Location of org-directory
(setq org-directory "~/Dropbox/org")

;; Easily open my daily journal file with this key binding
(global-set-key (kbd "C-c j") 
                (lambda () (interactive) (find-file "~/Dropbox/writing/write_every_day.org")))

;; I use C-c c to start capture mode
(global-set-key (kbd "C-c c") 'org-capture)

;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
(setq org-capture-templates
      (quote (("t" "todo" entry (file "~/Dropbox/org/notes.org")
               "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
              ("r" "respond" entry (file "~/Dropbox/org/notes.org")
               "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
              ("n" "note" entry (file "~/Dropbox/org/notes.org")
               "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
              ("j" "Journal" entry (file+datetree "~/Dropbox/org/notes.org")
               "* %?\n%U\n" :clock-in t :clock-resume t)
              ("w" "org-protocol" entry (file "~/Dropbox/org/notes.org")
               "* TODO Review %c\n%U\n" :immediate-finish t)
              ("m" "Meeting" entry (file "~/Dropbox/org/notes.org")
               "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
              ("p" "Phone call" entry (file "~/Dropbox/org/notes.org")
               "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
              ("h" "Habit" entry (file "~/Dropbox/org/notes.org")
               "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"%<<%Y-%m-%d %a .+1d/3d>>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

;; The following lines are always needed. Choose your own keys.
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
;; Automatically start Org-mode with "indented mode"
(setq org-startup-indented t)
;; "Clock" when I complete a task
(setq org-log-done 'time)
;; Set the workflow states for org-mode; enable single-key toggling
;; Short cut keys suggested by Sacha Chua, via email
(setq org-todo-keywords
       '((sequence "TODO(t)" "STARTED(s)" "CANCELLED(c)" "DONE(d)" )))
;; Now use the short cuts!
(setq org-use-fast-todo-selection t)
;; And set the colors for these workflow states
(setq org-todo-keyword-faces
           '(("TODO" . org-warning) ("STARTED" . "yellow")
             ("CANCELED" . (:foreground "blue" :weight bold))))

;; I want to see fancy bullets
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; Set up org-capture
(setq org-default-notes-file (concat org-directory "/notes.org"))
(define-key global-map "\C-cc" 'org-capture)

;; UTF-8 as default encoding
(set-language-environment "UTF-8")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("d677ef584c6dfc0697901a44b885cc18e206f05114c8a3b7fde674fce6180879" "9dad4b11367cda1041ed375ae2f7cc71558898f234edf4893498c5b22b283e56" "a1289424bbc0e9f9877aa2c9a03c7dfd2835ea51d8781a0bf9e2415101f70a7e" "7c996062565867025210d10a3ca9825c013251a118449d24948afc7b10470e6a" "90d329edc17c6f4e43dbc67709067ccd6c0a3caa355f305de2041755986548f2" default)))
 '(package-selected-packages
   (quote
    (solarized-theme org-bullets hydandata-light-theme paper-theme writeroom-mode zenburn-theme groovy-mode color-theme atom-dark-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
